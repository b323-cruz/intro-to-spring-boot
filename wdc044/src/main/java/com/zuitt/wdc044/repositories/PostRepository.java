package com.zuitt.wdc044.repositories;
import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// This is an interface containing behaviours that a class implements. An interface marked as @Repository contains methods for database manipulation. By extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, retrieving, updating, and deleting
public interface PostRepository extends CrudRepository<Post,Object> {
    Iterable<Post> findByUser_Id(Long userId);

}
