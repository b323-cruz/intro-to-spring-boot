package com.zuitt.wdc044.models;
import javax.persistence.*;

// We are marking this Java object as a representation of a database table via @Entity
@Entity
// We are simply designating the table name via @Table
@Table(name = "posts")
public class Post {
    // Indicates that this property represents the primary key via @Id
    @Id
    // Value for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    // Class properties that represent table columns in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    // constructors
    public Post(){}
    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    // getters and setters for variable title
    public String getTitle(){
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    // getters and setters for variable content
    public String getContent(){
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    // getters and setters for user
    public User getUser(){
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
