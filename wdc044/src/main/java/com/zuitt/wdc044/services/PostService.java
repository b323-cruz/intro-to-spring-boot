package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PostService {
    // Create / Add / Post
    void createPost(String stringToken, Post post);

    // Update / Edit / Put
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete
    ResponseEntity deletePost(Long id, String stringToken);

    // Read / Get
    Iterable<Post> getPosts();

    // Read / Get (but for specific authenticated user)
    Iterable<Post> getUserPosts(String stringToken);
}
