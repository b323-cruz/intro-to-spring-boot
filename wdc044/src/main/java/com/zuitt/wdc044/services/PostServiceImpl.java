package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    JwtToken jwtToken;

    // Create / Add / Post
    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    // Read / Get
    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    // Update / Edit / Put
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Delete
    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForRetrieving = postRepository.findById(id).get();
        String postAuthor = postForRetrieving.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUser.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Read / Get (but for specific authenticated user)
    public Iterable<Post> getUserPosts(String stringToken) {
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        if (user != null) {
            return postRepository.findByUser_Id(user.getId());
        } else {
            return null;
        }
    }
}
